# -*- coding: utf-8 -*-

"""Ce module affiche une scène très simple."""

import pygame as pg
from pygame import Color
from pygame.locals import *

from pgutil.pygame_scene import PygameScene
from robot.player_sprite import PlayerSprite, PlayerSpriteSolution
from robot.robot_sprite import RobotSprite, RobotSpriteSolution


# Complétez la scène ci-dessous
class RobotEscapeScene(PygameScene):
    """Dessine une scène très simple."""

    def __init__(self):
        pass

    def draw(self, surface):
        """Dessine la scène."""
        surface.fill(Color("black"))

class RobotEscapeSceneSolution(PygameScene):
    """Dessine une scène très simple."""

    def __init__(self):
        self.background = Color("black")
        self.player = PlayerSpriteSolution()
        robot = RobotSpriteSolution()
        self.robotGroup = pg.sprite.Group(robot)
        self.allGroup = pg.sprite.Group(self.player, robot)

    def draw(self, surface):
        """Dessine la scène."""
        surface.fill(self.background)
        self.allGroup.draw(surface)

    def update(self, deltat):
        """Met à jour la scène."""
        collisions = pg.sprite.spritecollide(self.player, self.robotGroup, False)
        if collisions:
            self.background = Color("white") if self.background == Color("black") else Color("black")
        self.allGroup.update(deltat)

    def on_event(self, event):
        """Gère l'évènement Pygame.
        :param pygame.event.Event event: évènement à traiter
        """
        if not hasattr(event, 'key'):
            return
        if event.key == K_UP:
            self.player.up()
        elif event.key == K_RIGHT:
            self.player.right()
        elif event.key == K_DOWN:
            self.player.down()
        elif event.key == K_LEFT:
            self.player.left()
